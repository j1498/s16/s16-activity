console.log("Hello World");


let value = parseInt(prompt("Provide a number: "))
console.log("Provided number: " + value)


for (let i = value; i--; i <= 0 ){

	if(i <= 50){
		console.log("The current value is at 50. Terminating the loop. ")
		break;
	}
	if (i % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number ")
		continue;
	}

	if (i % 5 === 0){
		console.log(i)
	}
}


// Stretch Goals

let myString = "supercalifragilisticexpialidocious";
let cons='';

console.log(myString);

for(let i = 0; i < myString.length; i++){
	if(
		myString[i] == 'a' ||
		myString[i] == 'e' ||
		myString[i] == 'i' ||
		myString[i] == 'o' ||
		myString[i] == 'u'
		) {
		continue;
	} else {
		cons += myString[i];
	}
}

console.log(cons);
